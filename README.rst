*pypres-gen* is a Python application for generating a PowerPoint (.pptx) presentation using
documents stored in a NoSQL database. Currently `MongoDB`_ is supported

The application makes use of the `python-pptx library`_

.. _`python-pptx library`:
   https://python-pptx.readthedocs.org/en/latest/

.. _`MongoDB`:
   https://mongodb.com