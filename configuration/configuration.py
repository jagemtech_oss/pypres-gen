import yaml
import logging
import os
import sys
from collections import UserDict

log = logging.getLogger(__name__)


class Configuration(UserDict):

    def __init__(self, config_path):
        self.config_path = os.path.expanduser(config_path)
        self._load()

    def _load(self):
        try:
            with open(os.path.expanduser(self.config_path), 'r') as file:
                try:
                    self.data = yaml.safe_load(file)
                except yaml.YAMLError as e:
                    log.error(
                        'Error parsing yaml of configuration file '
                        '{}'.format(
                            e.__cause__
                        )
                    )
                    sys.exit(1)
        except FileNotFoundError:
            log.error(
                'Error opening configuration file {}'.format(self.config_path)
            )
            sys.exit(1)

    def get(self, key):
        """
        Fetch the configuration value of the specified key. If there are nested
        dictionaries, a dot notation can be used.

        So if the configuration contents are:

        data = {
            'first': {
                'second': 'value'
            },
        }

        data.get('first.second') == 'value'

        Arguments:
            key(str): Configuration key to fetch
        """
        keys = key.split('.')
        value = self.data.copy()

        for key in keys:
            value = value[key]

        return value


