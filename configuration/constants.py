import os

# constant values
CONFIG_MODULE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = os.path.abspath(os.path.join(CONFIG_MODULE_DIR, os.pardir))
TEMPLATES_DIR = os.path.join(ROOT_DIR, 'templates') + '/'

# configuration key constants
APP_CONFIG_FILE = 'config.yml'
OUTPUT_DIR_KEY = 'output_dir'
