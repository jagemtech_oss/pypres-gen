.. pypres-gen documentation master file, created by
   sphinx-quickstart on Tue Sep  6 14:09:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pypres-gen's documentation!
======================================

.. include:: ../../README.rst


Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
