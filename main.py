from service.presentation import PresentationService
import sys
import logging
from configuration.configuration import Configuration
from configuration import constants

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def do(key):
    # load application config
    app_config = Configuration(constants.APP_CONFIG_FILE)
    prs_service = PresentationService(key, app_config)

    # create the PowerPoint presentation
    log.info(
        'Creating presentation {} '.format(key)
    )
    prs_service.create_presentation()


if __name__ == "__main__":
    try:
        arg = sys.argv[1]
        do(arg)
    except IndexError:
        raise SystemExit(f"Usage: {sys.argv[0]} <presentation id>")
