from mongoengine import *

connect(host="mongodb://127.0.0.1:27017/ppt")


class PlaceholderModel(EmbeddedDocument):
    idx = IntField(db_field='idx')
    placeholder_type = StringField(db_field='type')
    content_provider = StringField(max_length=50)
    content = StringField()


class SlideModel(EmbeddedDocument):
    title = StringField(max_length=50)
    layout_index = IntField(db_field='layout_index')
    placeholders = ListField(EmbeddedDocumentField('PlaceholderModel'))


class PresentationModel(Document):

    meta = {'collection': 'presentation'}
    key = StringField(required=True)
    template = StringField(required=True)
    title = StringField(max_length=50)
    subtitle = StringField(max_length=50)
    filename = StringField(max_length=50)
    slides = ListField(EmbeddedDocumentField('SlideModel'))



