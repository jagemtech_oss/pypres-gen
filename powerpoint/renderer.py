from __future__ import annotations
from abc import ABC, abstractmethod

"""Classes that render content into instances of python-pptx Shape (Placeholder) related classes.

Classes that inherit from BasePlaceholderRenderer should encapsulate the logic
that takes a data structure and inserts into a slide Placeholder instance
"""


class BasePlaceholderRenderer(ABC):

    @abstractmethod
    def render(self, placeholder, data):
        """
        Subclasses must implement this method to enable rendering of content into a Placeholder
        Arguments:
            placeholder: an implementation of a Placeholder Shape
            data: the data to be inserted into the Placeholder
        """
        pass


class TablePlaceholderRenderer(BasePlaceholderRenderer):

    def render(self, placeholder, data):
        rows = len(data)
        cols = len(data[0])
        data_table = placeholder.insert_table(rows, cols).table
        row_idx = 0
        for row in data:
            col_idx = 0
            for col in row:
                data_table.cell(row_idx, col_idx).text = col
                col_idx += 1
            row_idx += 1


class TextPlaceholderRenderer(BasePlaceholderRenderer):

    L1_BULLET_LIST_IDX = 1

    def render(self, placeholder, data):
        tf = placeholder.text_frame
        paragraph = data['para']
        tf.text = paragraph
        list_items = data['level_1']
        for item in list_items:
            p = tf.add_paragraph()
            p.text = item
            p.level = self.L1_BULLET_LIST_IDX
