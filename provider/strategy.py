from __future__ import annotations
from provider.transformer import HtmlContentTransformer
from provider.transformer import CsvContentTransformer
from powerpoint.renderer import TextPlaceholderRenderer
from powerpoint.renderer import TablePlaceholderRenderer
from abc import ABC, abstractmethod
import os
import sys
import csv
import logging

log = logging.getLogger(__name__)


def load_content_file(root_dir, file_name):
    try:
        file_path = os.path.join(root_dir, file_name)
        content_file = open(file_path, "r")
    except FileNotFoundError:
        log.error(
            'Error opening file {} in source directory {}'.format(file_name, root_dir)
        )
        raise
    return content_file


"""Content provider strategy related classes.

Classes that inherit from BaseContentProvider should encapsulate the logic
that loads raw content/data from a stream (e.g. HTML page or CSV file), instantiates the appropriate
concrete BaseContentTransformer implementation and invokes its transform method with the required arguments
"""


class BaseContentProvider(ABC):
    PLACEHOLDER_TYPE_TEXT = 'text'
    PLACEHOLDER_TYPE_TABLE = 'table'
    PLACEHOLDER_TYPE_CHART = 'chart'

    renderers = {PLACEHOLDER_TYPE_TEXT: TextPlaceholderRenderer(), PLACEHOLDER_TYPE_TABLE: TablePlaceholderRenderer()}

    @abstractmethod
    def execute(self, content_placeholder, content):
        """
        Subclasses must implement this method to execute an appropriate strategy for the supplied configuration
        Arguments:
            content_placeholder: a Placeholder implementation within pptx.shapes.placeholder
            content: the content to transform and insert into the placeholder
        """
        pass


class CsvContentProvider (BaseContentProvider):

    def execute(self, content_placeholder, content):
        lines = content.splitlines()
        reader = csv.reader(lines)
        parsed_csv = list(reader)
        csv_content_transformer = CsvContentTransformer(parsed_csv)
        csv_content_transformer.transform(self.renderers[self.PLACEHOLDER_TYPE_TABLE], content_placeholder)


class HtmlContentProvider (BaseContentProvider):

    def execute(self, content_placeholder, html):
        html_content_transformer = HtmlContentTransformer(html)
        html_content_transformer.transform(self.renderers[self.PLACEHOLDER_TYPE_TEXT], content_placeholder)

