from __future__ import annotations
from abc import ABC, abstractmethod
from bs4 import BeautifulSoup

"""Content transformer related classes.

Classes that inherit from BaseContentTransformer should encapsulate the logic
that takes raw content/data (e.g. HTML document or CSV data) and converts it into appropriate data structures for use
by a supplied concrete BasePlaceholderRenderer implementation
"""


class BaseContentTransformer(ABC):

    def __init__(self, data) -> None:
        super().__init__()
        self.data = data

    @abstractmethod
    def transform(self, placeholder_renderer, placeholder):
        """
        Subclasses must implement this method to enable transformation of content
        Arguments:
            placeholder_renderer: an implementation of BasePlaceHolderRenderer
            placeholder: the placeholder to be populated with content by the renderer
        """
        pass

    def set_data(self, data):
        self.data = data


class CsvContentTransformer(BaseContentTransformer):

    def __init__(self, data) -> None:
        super().__init__(data)

    def transform(self, placeholder_renderer, placeholder):
        placeholder_renderer.render(placeholder, self.data)


class HtmlContentTransformer(BaseContentTransformer):

    SOUP_HTML_PARSER = 'html.parser'

    def __init__(self, data=None) -> None:
        super().__init__(data)
        if data is not None:
            self._soup = BeautifulSoup(data, self.SOUP_HTML_PARSER)

    def set_soup(self, soup):
        self._soup = soup

    def transform(self, placeholder_renderer, placeholder):
        placeholder_renderer.render(placeholder, self._get_content_from_html())

    def _get_content_from_html(self):
        para = self._soup.find_all("p")[0].text
        list_items = self._soup.find_all("li")
        level_1 = []
        for item in list_items:
            level_1.append(item.text)
        return {'para': para, 'level_1': level_1}