from model.presentation import PresentationModel
from provider.strategy import BaseContentProvider, HtmlContentProvider, CsvContentProvider
from pptx import Presentation
from configuration import constants
import os
import logging

log = logging.getLogger(__name__)


class PresentationService:

    LAYOUT_TITLE = 0
    content_providers = {
        'html': HtmlContentProvider(),
        'csv': CsvContentProvider()
    }

    def __init__(self, _id, app_config) -> None:
        super().__init__()
        self.prs_model = self._get_presentation_model(_id)
        template = constants.TEMPLATES_DIR + self.prs_model.template
        self.prs = Presentation(template)
        self.app_config = app_config

    @staticmethod
    def _get_presentation_model(key):
        log.info(
            'Retrieving presentation model for {}'.format(key)
        )
        return PresentationModel.objects(key=key).first()

    def _add_title_slide(self):
        title_slide_layout = self.prs.slide_layouts[self.LAYOUT_TITLE]
        slide = self.prs.slides.add_slide(title_slide_layout)
        title = slide.shapes.title
        subtitle = slide.placeholders[1]
        title.text = self.prs_model.title
        subtitle.text = self.prs_model.subtitle

    def _save(self):
        output_dir = self.app_config.get(constants.OUTPUT_DIR_KEY)
        out_file = os.path.join(output_dir, self.prs_model.filename)
        log.info(
            'Saving presentation {}'.format(out_file)
        )
        self.prs.save(out_file)

    def _add_slide(self, slide_model):
        log.info(
            'Adding slide \'{}\' using layout index {}'.format(slide_model.title, slide_model.layout_index)
        )
        slide_layout = self.prs.slide_layouts[slide_model.layout_index]
        slide = self.prs.slides.add_slide(slide_layout)
        shapes = slide.shapes
        title = shapes.title
        title.text = slide_model.title
        for placeholder_model in slide_model.placeholders:
            self._add_content(slide, placeholder_model)

    def _add_content(self, shapes, placeholder_model):
        log.info(
            'Adding {} content to placeholder {}'.format(placeholder_model.placeholder_type, placeholder_model.idx)
        )
        content_placeholder = shapes.placeholders[placeholder_model.idx]
        content_provider = self._get_placeholder_content_provider(placeholder_model.content_provider)
        content_provider.execute(content_placeholder, placeholder_model.content)

    def _get_placeholder_content_provider(self, content_provider_key) -> BaseContentProvider:
        return self.content_providers.get(content_provider_key)

    def create_presentation(self):
        self._add_title_slide()
        slides = self.prs_model.slides
        for slide in slides:
            self._add_slide(slide)
        self._save()



