import unittest
from unittest.mock import patch, call, PropertyMock
from provider.strategy import CsvContentProvider


class test_LocalCsvContentProvider(unittest.TestCase):

    def setUp(self) -> None:
        self.class_under_test = CsvContentProvider()

    @patch('pptx.shapes.placeholder._BaseSlidePlaceholder')
    def test_execute_invalid_content_source(self, mock_placeholder):
        # ppt_slide_placeholder = PptSlidePlaceholder(1, 'table', 'csv', 'nonexistent.csv')
        pass
        

if __name__ == '__main__':
    unittest.main()
