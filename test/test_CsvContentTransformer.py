import unittest
from unittest.mock import patch, call
from provider.transformer import CsvContentTransformer


class test_CsvContentTransformer (unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.data = [[1, 2, 3], [4, 5, 6]]
        self.class_under_test = CsvContentTransformer(self.data)

    @patch('powerpoint.renderer.TablePlaceholderRenderer')
    @patch('pptx.shapes.placeholder.TablePlaceholder')
    def test_transform(self, mock_placeholder_renderer, mock_placeholder):
        self.class_under_test.transform(mock_placeholder_renderer, mock_placeholder)
        expected_data = self.data
        mock_placeholder_renderer.render.assert_called()
        mock_placeholder_renderer.render.assert_called_once()
        mock_placeholder_renderer.render.assert_called_with(mock_placeholder, expected_data)


if __name__ == '__main__':
    unittest.main()