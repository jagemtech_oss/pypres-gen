import unittest
from unittest.mock import patch, call
from provider.transformer import HtmlContentTransformer
from bs4 import BeautifulSoup


class test_HtmlContentTransformer (unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.class_under_test = HtmlContentTransformer()

    @patch('powerpoint.renderer.TextPlaceholderRenderer')
    @patch('pptx.shapes.placeholder.BasePlaceholder')
    def test_transform(self, mock_placeholder_renderer, mock_placeholder):
        html_para = "<p>Simple paragraph</p><ul><li>item1</li><li>item2</li></ul>"
        self.class_under_test.set_soup(BeautifulSoup(html_para, self.class_under_test.SOUP_HTML_PARSER))
        self.class_under_test.transform(mock_placeholder_renderer, mock_placeholder)
        expected_data = {'para': 'Simple paragraph', 'level_1': ['item1', 'item2']}
        mock_placeholder_renderer.render.assert_called()
        mock_placeholder_renderer.render.assert_called_once()
        mock_placeholder_renderer.render.assert_called_with(mock_placeholder, expected_data)


if __name__ == '__main__':
    unittest.main()
