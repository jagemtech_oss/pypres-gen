import unittest
from mongoengine import connect, disconnect, Document, StringField
from model.presentation import PresentationModel


class test_PresentationModel (unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        disconnect()
        connect('mongoenginetest', host='mongomock://localhost')

    @classmethod
    def tearDownClass(cls):
        pass
        #disconnect()

    def test_get_all(self):
        pm = PresentationModel(title='foo', template='bar', subtitle='foobar', filename='barfoo', key='key1')
        pm.save()

        p = PresentationModel.objects().first()
        self.assertTrue(p is not None)
        self.assertEqual('foo', p.title)
        self.assertEqual('bar', p.template)
        self.assertEqual('foobar', p.subtitle)
        self.assertEqual('barfoo', p.filename)
        self.assertEqual('key1', p.key)
