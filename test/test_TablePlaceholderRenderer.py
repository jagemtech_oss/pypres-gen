import unittest
from unittest.mock import patch, call
from powerpoint.renderer import TablePlaceholderRenderer


class test_TablePlaceholderRenderer(unittest.TestCase):

    def setUp(self) -> None:
        self.table_data = [['col1', 'col2'], [1, 2], [3, 4]]
        self.class_under_test = TablePlaceholderRenderer()

    @patch('pptx.shapes.placeholder.TablePlaceholder')
    def test_render(self, mock_placeholder):
        self.class_under_test.render(mock_placeholder, self.table_data)
        mock_placeholder.insert_table.assert_called()
        expected_rows = 3
        expected_cols = 2
        mock_placeholder.insert_table.assert_called_with(expected_rows, expected_cols)


if __name__ == '__main__':
    unittest.main()
