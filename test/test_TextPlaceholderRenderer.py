import unittest
from unittest.mock import patch, call, PropertyMock
from powerpoint.renderer import TextPlaceholderRenderer


class test_TextPlaceholderRenderer(unittest.TestCase):

    def setUp(self) -> None:
        self.text_data = {'para': 'Simple paragraph', 'level_1': ['item1', 'item2']}
        self.class_under_test = TextPlaceholderRenderer()

    @patch('pptx.shapes.placeholder._BaseSlidePlaceholder')
    def test_render(self, mock_placeholder):
        p = PropertyMock()
        type(mock_placeholder).text_frame = p
        self.class_under_test.render(mock_placeholder, self.text_data)
        p.assert_called()
        

if __name__ == '__main__':
    unittest.main()
